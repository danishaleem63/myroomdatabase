package com.danishp.myroomdatabase.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.danishp.myroomdatabase.interfaces.DaoAccess;
import com.danishp.myroomdatabase.model.StarredData;

@Database(entities = {StarredData.class}, version = 1, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {
    public abstract DaoAccess daoAccess();
}
