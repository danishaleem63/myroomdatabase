package com.danishp.myroomdatabase.Database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;

import com.danishp.myroomdatabase.model.StarredData;

import java.util.List;

public class StarredRepository {

    private String DB_NAME = "MyRoomDB";

    private MyDatabase myDatabase;

    public StarredRepository(Context context) {
        myDatabase = Room.databaseBuilder(context, MyDatabase.class, DB_NAME).build();
    }

    public void insertTask(final String matchID, final String name, final String address, final String city) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                myDatabase.daoAccess().insertTask(new StarredData(matchID, name, address, city));
            }
        });
    }

    public LiveData<List<StarredData>> getTasks() {
        return myDatabase.daoAccess().fetchAllTasks();
    }

    private StarredData getTask(String id) {
        return myDatabase.daoAccess().getTask(id);
    }

    public void deleteTask(final String id) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final StarredData starredDataLiveData = getTask(id);
                if (starredDataLiveData != null) {
                    myDatabase.daoAccess().deleteTask(starredDataLiveData);
                }
            }
        });
    }
}
