package com.danishp.myroomdatabase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danishp.myroomdatabase.adapter.AllMatchesAdapter;
import com.danishp.myroomdatabase.model.StarredData;
import com.danishp.myroomdatabase.model.Venue;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class AllMatchesFragment extends Fragment {
    private List<Venue> data;
    private List<StarredData> dbList;
    private Context context;
    private RecyclerView rv_Allmatches;


    public AllMatchesFragment(List<Venue> data, HomeActivity mainActivity, List<StarredData> starredData) {
        this.data = data;
        this.context = mainActivity;
        this.dbList = starredData;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.allmatcheslayout, container, false);

        rv_Allmatches = view.findViewById(R.id.rv_Allmatches);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context,DividerItemDecoration.VERTICAL);
        rv_Allmatches.addItemDecoration(dividerItemDecoration);
        rv_Allmatches.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        AllMatchesAdapter adapter = new AllMatchesAdapter(data,context,dbList);
        rv_Allmatches.setAdapter(adapter);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("All Matches");
    }
}
