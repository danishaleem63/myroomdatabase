package com.danishp.myroomdatabase;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.danishp.myroomdatabase.Database.StarredRepository;
import com.danishp.myroomdatabase.interfaces.GetAllMatchesAPI;
import com.danishp.myroomdatabase.model.StarredData;
import com.danishp.myroomdatabase.model.Venue;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    List<Venue> data = new ArrayList<>();
    ProgressBar progressBar;
    List<StarredData> dbList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupDrawerMenu(toolbar);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        getStarredData();
        getData();
    }

    private void getStarredData() {
        if(!dbList.isEmpty()){
            dbList.clear();
        }
        StarredRepository starredRepository = new StarredRepository(getApplicationContext());
        starredRepository.getTasks().observe(this, new Observer<List<StarredData>>() {
            @Override
            public void onChanged(@Nullable List<StarredData> data) {
                if(data!=null){
                    for(StarredData starredData1 : data) {
                        System.out.println("-----------------------");
                        System.out.println(starredData1.getMatchID());
                        System.out.println(starredData1.getAddress());
                        System.out.println(starredData1.getCity());
                        System.out.println(starredData1.getName());
                        dbList.add(starredData1);
                    }
                }
            }
        });
    }

    private void setupDrawerMenu(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_all) {
            getStarredDataForMatchFragment();
        } else if (id == R.id.nav_star) {
            getStarredDataForStarredFragment();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getStarredDataForStarredFragment() {
        if(!dbList.isEmpty()){
            dbList.clear();
        }
        StarredRepository starredRepository = new StarredRepository(getApplicationContext());
        starredRepository.getTasks().observe(this, new Observer<List<StarredData>>() {
            @Override
            public void onChanged(@Nullable List<StarredData> dataStarred) {
                if(dataStarred!=null){
                    for(StarredData starredData1 : dataStarred) {
                        System.out.println("-----------------------");
                        System.out.println(starredData1.getMatchID());
                        System.out.println(starredData1.getAddress());
                        System.out.println(starredData1.getCity());
                        System.out.println(starredData1.getName());
                        dbList.add(starredData1);
                    }
                    StarFragment fragment = new StarFragment(HomeActivity.this,dbList,data);
                    loadFragment(fragment);
                }
            }
        });
    }

    private void getStarredDataForMatchFragment() {
        if(!dbList.isEmpty()){
            dbList.clear();
        }
        StarredRepository starredRepository = new StarredRepository(getApplicationContext());
        starredRepository.getTasks().observe(this, new Observer<List<StarredData>>() {
            @Override
            public void onChanged(@Nullable List<StarredData> dataStarred) {
                if(dataStarred!=null){
                    for(StarredData starredData1 : dataStarred) {
                        System.out.println("-----------------------");
                        System.out.println(starredData1.getMatchID());
                        System.out.println(starredData1.getAddress());
                        System.out.println(starredData1.getCity());
                        System.out.println(starredData1.getName());
                        dbList.add(starredData1);
                    }
                    AllMatchesFragment fragment = new AllMatchesFragment(data,HomeActivity.this,dbList);
                    loadFragment(fragment);
                }
            }
        });
    }

    @SuppressLint("ResourceType")
    private Boolean loadFragment(Fragment fragment){
        if(fragment!=null){
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout,fragment).commit();
            return true;
        }else{
            return false;
        }
    }

    private void getData() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(GetAllMatchesAPI.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

        GetAllMatchesAPI allMatchesAPI = retrofit.create(GetAllMatchesAPI.class);
        Call<JsonObject> call = allMatchesAPI.getData();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Gson gson = new Gson();
                Type type = new TypeToken<List<Venue>>(){}.getType();
                data = gson.fromJson(response.body().get("response").getAsJsonObject().getAsJsonArray("venues"), type);
                progressBar.setVisibility(View.GONE);

                AllMatchesFragment fragment = new AllMatchesFragment(data,HomeActivity.this, dbList);
                loadFragment(fragment);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(HomeActivity.this, "Something went wrong try after some time", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

}
