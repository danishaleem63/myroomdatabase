
package com.danishp.myroomdatabase.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BeenHere implements Serializable
{

    @SerializedName("lastCheckinExpiredAt")
    @Expose
    private int lastCheckinExpiredAt;
    private final static long serialVersionUID = -6093042563247473889L;

    public int getLastCheckinExpiredAt() {
        return lastCheckinExpiredAt;
    }

    public void setLastCheckinExpiredAt(int lastCheckinExpiredAt) {
        this.lastCheckinExpiredAt = lastCheckinExpiredAt;
    }

}
