package com.danishp.myroomdatabase.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class StarredData {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    private String matchID;
    private String name;
    private String address;
    private String city;

    public StarredData(String matchID, String name, String address, String city) {
        this.matchID = matchID;
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public String getMatchID() {
        return matchID;
    }

    public void setMatchID(String matchID) {
        this.matchID = matchID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
