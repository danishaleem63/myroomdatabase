
package com.danishp.myroomdatabase.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stats implements Serializable
{

    @SerializedName("tipCount")
    @Expose
    private int tipCount;
    @SerializedName("usersCount")
    @Expose
    private int usersCount;
    @SerializedName("checkinsCount")
    @Expose
    private int checkinsCount;
    private final static long serialVersionUID = -1748938779471862560L;

    public int getTipCount() {
        return tipCount;
    }

    public void setTipCount(int tipCount) {
        this.tipCount = tipCount;
    }

    public int getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(int usersCount) {
        this.usersCount = usersCount;
    }

    public int getCheckinsCount() {
        return checkinsCount;
    }

    public void setCheckinsCount(int checkinsCount) {
        this.checkinsCount = checkinsCount;
    }

}
