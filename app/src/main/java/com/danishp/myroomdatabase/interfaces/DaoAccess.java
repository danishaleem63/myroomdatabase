package com.danishp.myroomdatabase.interfaces;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.danishp.myroomdatabase.model.StarredData;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    Long insertTask(StarredData starredData);

    @Query("SELECT * FROM StarredData")
    LiveData<List<StarredData>> fetchAllTasks();

    @Delete
    void deleteTask(StarredData starredData);

    @Query("SELECT * FROM StarredData WHERE matchID = :taskId")
    StarredData getTask(String taskId);
}
