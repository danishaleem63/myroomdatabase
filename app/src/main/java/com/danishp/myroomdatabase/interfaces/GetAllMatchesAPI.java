package com.danishp.myroomdatabase.interfaces;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetAllMatchesAPI {

    String BASE_URL = "https://api.foursquare.com/v2/venues/";

    @GET("search?ll=40.7484,-73.9857&oauth_token=NPKYZ3WZ1VYMNAZ2FLX1WLECAWSMUVOQZOIDBN53F3LVZBPQ&v=20180616")
    Call<JsonObject> getData();
}
