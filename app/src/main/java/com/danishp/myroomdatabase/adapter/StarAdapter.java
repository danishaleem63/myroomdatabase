package com.danishp.myroomdatabase.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.danishp.myroomdatabase.Database.StarredRepository;
import com.danishp.myroomdatabase.R;
import com.danishp.myroomdatabase.model.StarredData;
import com.danishp.myroomdatabase.model.Venue;

import java.util.ArrayList;
import java.util.List;

public class StarAdapter extends RecyclerView.Adapter<StarAdapter.viewHolder> {


    private List<StarredData> dbList ;
    private StarredRepository starredRepository;
    private Context context;
    List<Venue> data;

    public StarAdapter(List<StarredData> dbList, Context context, List<Venue> data) {
        this.dbList = dbList;
        this.context = context;
        this.starredRepository = new StarredRepository(context);
        this.data = data;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.allmatchesadapterlayout, viewGroup, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final viewHolder viewHolder, final int position) {
        viewHolder.tv_id.setText("Id : "+dbList.get(position).getMatchID());
        viewHolder.tv_name.setText("Name : "+dbList.get(position).getName());
        viewHolder.tv_address.setText("Address : "+dbList.get(position).getAddress());
        viewHolder.tv_city.setText("City & State : "+dbList.get(position).getCity());

        viewHolder.iv_star.setVisibility(View.GONE);
        viewHolder.iv_star1.setVisibility(View.VISIBLE);

        viewHolder.iv_star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < data.size(); i++) {
                    if(dbList.get(position).getMatchID().equals(data.get(i).getId())){
                        data.get(i).setStarred(false);
                    }
                }
                StarredData venueModel = dbList.get(position);
                starredRepository.deleteTask(venueModel.getMatchID());
                viewHolder.iv_star1.setVisibility(View.GONE);
                viewHolder.iv_star.setVisibility(View.VISIBLE);
                dbList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        if(dbList==null){
            return 0;
        }else{
            return dbList.size();
        }
    }

    class viewHolder extends RecyclerView.ViewHolder {

        TextView tv_id, tv_name, tv_address, tv_city;
        ImageView iv_star,iv_star1;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            tv_id = itemView.findViewById(R.id.tv_id);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_city = itemView.findViewById(R.id.tv_city);
            iv_star = itemView.findViewById(R.id.iv_star);
            iv_star1 = itemView.findViewById(R.id.iv_star1);
        }
    }
}
