package com.danishp.myroomdatabase.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.danishp.myroomdatabase.Database.StarredRepository;
import com.danishp.myroomdatabase.R;
import com.danishp.myroomdatabase.model.StarredData;
import com.danishp.myroomdatabase.model.Venue;

import java.util.ArrayList;
import java.util.List;

public class AllMatchesAdapter extends RecyclerView.Adapter<AllMatchesAdapter.viewHolder> {


    private List<Venue> data;
    private Context context;
    private StarredRepository starredRepository;
    private List<StarredData> dbList;


    public AllMatchesAdapter(List<Venue> data, Context context, List<StarredData> dbList) {
        this.data = data;
        this.context = context;
        this.starredRepository = new StarredRepository(context);
        this.dbList = dbList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.allmatchesadapterlayout, viewGroup, false);
        return new viewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final viewHolder viewHolder, final int position) {
        viewHolder.tv_id.setText("Id : " + data.get(position).getId());
        viewHolder.tv_name.setText("Name : " + data.get(position).getName());
        viewHolder.tv_address.setText("Address : " + data.get(position).getLocation().getAddress());
        viewHolder.tv_city.setText("City & State : " + data.get(position).getLocation().getCity() + "'" + data.get(position).getLocation().getState());

        if (dbList != null) {
            for (int i = 0; i < dbList.size(); i++) {
                if (dbList.get(i).getMatchID().equals(data.get(position).getId())) {
                    data.get(position).setStarred(true);
                }
            }
        }

        if (data.get(position).isStarred()) {
            viewHolder.iv_star1.setVisibility(View.VISIBLE);
            viewHolder.iv_star.setVisibility(View.GONE);
        } else {
            viewHolder.iv_star1.setVisibility(View.GONE);
            viewHolder.iv_star.setVisibility(View.VISIBLE);
        }

        viewHolder.iv_star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Venue venueModel = data.get(position);
                starredRepository.insertTask(venueModel.getId(), venueModel.getName(), venueModel.getLocation().getAddress(), venueModel.getLocation().getCity());
                venueModel.setStarred(true);
                viewHolder.iv_star1.setVisibility(View.VISIBLE);
                viewHolder.iv_star.setVisibility(View.GONE);
            }
        });

        viewHolder.iv_star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Venue venueModel = data.get(position);
                starredRepository.deleteTask(venueModel.getId());
                venueModel.setStarred(false);
                viewHolder.iv_star1.setVisibility(View.GONE);
                viewHolder.iv_star.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class viewHolder extends RecyclerView.ViewHolder {

        TextView tv_id, tv_name, tv_address, tv_city;
        ImageView iv_star, iv_star1;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            tv_id = itemView.findViewById(R.id.tv_id);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_city = itemView.findViewById(R.id.tv_city);
            iv_star = itemView.findViewById(R.id.iv_star);
            iv_star1 = itemView.findViewById(R.id.iv_star1);
        }
    }
}
