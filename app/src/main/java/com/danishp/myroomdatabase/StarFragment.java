package com.danishp.myroomdatabase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danishp.myroomdatabase.adapter.StarAdapter;
import com.danishp.myroomdatabase.model.StarredData;
import com.danishp.myroomdatabase.model.Venue;


import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class StarFragment extends Fragment {

    List<StarredData> dbList;
    Context context;
    RecyclerView rv_Allmatches;
    List<Venue> data;

    public StarFragment(Context context, List<StarredData> dbList, List<Venue> data) {
        this.dbList = dbList;
        this.context = context;
        this.data = data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.starfragmentlayout, container, false);

        rv_Allmatches = view.findViewById(R.id.rv_Allmatches);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context,DividerItemDecoration.VERTICAL);
        rv_Allmatches.addItemDecoration(dividerItemDecoration);
        rv_Allmatches.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        StarAdapter adapter = new StarAdapter(dbList,context,data);
        rv_Allmatches.setAdapter(adapter);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Stared Marked");
    }
}
